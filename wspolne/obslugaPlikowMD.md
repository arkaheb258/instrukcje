# Wstęp  
Instrukcja wewnętrzna  

**Obsługa plików w formacie .md (markdown)**  

Autor: Paweł Karp   


# Wersja  
v1.0.0 - 2015-12-02 - Stworzenie dokumentu.      


# Instrukcja  
### Opis  
Markdown – język znaczników, został stworzony w celu uproszczenia tworzenia i formatowania tekstu.  
Łatwo można go przekonwertować na kod HTML. Jest najczęściej używany do tworzenia krótkich plików informacyjnych, help itp.  
Więcej informacji: [**wikipedia**](https://pl.wikipedia.org/wiki/Markdown)  

### Przeglądanie offline  
W celu zachowania plików do przeglądania plików lokalnie na laptopie:  
- wejść na stronę do przeglądania instrukcji: [**https://bitbucket.org/pawelprak/instrukcje **](https://bitbucket.org/pawelprak/instrukcje)   
- z menu po lewej stronie kliknąć ** Source **  
- na górze będzie widoczna struktura katalogów z instrukcjami - znawigować do katalogu z instrukcją, która ma być ściągnięta, np. ** ktw **  
- prawym przyskiem myszy kliknąć na instrukcji, wybrać ** Zapisz link jako... ***  
- powinno pojawić się okno w wyborem miejsca do zapisu na komputerze, zapisywny plik będzie miał rozszerzenie *.htm  
- plik można otworzyć potem przeglądarką internetową  

### Drukowanie do pdf
- wejść na stronę do przeglądania instrukcji: [**https://bitbucket.org/pawelprak/instrukcje **](https://bitbucket.org/pawelprak/instrukcje)   
- otworzyć instrukcję, w prawym górnym rogu kliknąć przycisk ** Blame **  
- skopiować cały tekst z edytora  
- otworzyć edytor online [**http://dillinger.io/ **](http://dillinger.io/)  
- do lewego okna wkleić skopiowany tekst, po prawej wyświetli się tekst sformatowany  
- w prawym górnym rogu wybrać z menu ** Export As **, wybrać opcję PDF  


