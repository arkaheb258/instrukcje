# Wstęp
Instrukcja wewnętrzna  

Stacja Transmisji Danych  
Komputer Bernecker PP500  
**Wgrywanie systemu operacyjnego Windows Embedded Standard 7**  

Autor: Paweł Karp    


# Wersja
v1.3: 2015-04-17 - Tą wersję dokumentacji przeniesiono na GIT.  


# Instrukcja
## Stworzenie bootowalnego dysku pendrive
System operacyjny monitora Stacji Transmisji Danych będzie wgrywany bezpośrednio z bootowalnego dysku pendrive (4GB minimum).  
Przygotowanie pendrive (pod Windows 7):  
- Włożyć pendrive do komputera
- Uruchomić  interpreter poleceń CMD:
    - wejść do menu START,  
    - w polu „Wyszukaj programy i pliki” wpisujemy `cmd`,
    - prawym przyciskiem myszy wybieramy „Uruchom jako administrator”,    
- W CMD uruchomić program do tworzenia partycji, należy wpisać komendę  
`> diskpart` - następnie po uruchomieniu programu wpisywać następujące polecenia:  
`> list disk` 		- sprawdzenie jaki numer dysku ma nasz pendrive,   
`> select disk #`	- wpisać numer dysku pendrive’a, np.  `select disk 1`,  
`> clean`  
`> create partition primary`  
`> select partition 1`  
`> format fs=ntfs quick`  
`> active`  
`> assign`  
`> exit`  
- Zamknąć CMD
- Na pendrive skopiować system operacyjny który znajduje się na serwerze:  

*192.168.30.12\Oprogramowanie dla produkcji\Paweł Karp\06   -   STD-1\ Kopex Machinery STD v1.3*   

Wgrać system z najwyższym numerem wersji.   
System można przegrać za pomocą eksploratora Windows (należy pamiętać aby włączyć widoczność ukrytych plików!).


## Instalacja systemu operacyjnego
### Zmiana ustawień BIOS
Aby rozpocząć instalację systemu należy w BIOS ustawić odpowiednią kolejność urządzeń z których kolejno będzie podejmowana próba uruchomienia systemu operacyjnego.  
W tym przypadku należy wskazać pierwsze urządzenie na pendrive w gnieździe USB.   

Włożyć pendrive z systemem do gniazda USB w monitorze (można wykorzystać wejście na froncie monitora w prawym dolnym rogu – należy odkręcić zaślepkę).  
Podpiąć klawiaturę oraz myszkę.  

Dostęp do ustawień BIOS uzyskujemy poprzez wciśnięcie klawisza F2 podczas uruchomienia komputera.  
Wymagane ustawienia (istotne są dwie pierwsze pozycje z poniższej listy):  

>BOOT / Legacy / Boot Type Order:   
- USB  
- Hard Disk Drive  
- Others  
- CD/DVD ROM Drive  
- Floppy Drive  

Ustawienia godziny zmienia się na głównej planszy:  
>- MAIN / System Time  
- MAIN / System Date

Należy podać czas uniwersalny UTC+0:00 - bez przesunięcia dla polskiej strefy czasowej.

Po ustawieniu odpowiedniej kolejności należy wyjść z BIOS za pomocą klawisza F10 potwierdzając chęć zapisu dokonanych zmian.


### Zainstalowanie systemu Windows Embedded 7
Podczas instalacji systemu operacyjnego Windows 7 Embedded pokaże się okno wyboru w którym  należy wybrać dysk na którym ma zostać zainstalowany system.  

Wybieramy dysk o pojemności ~ 16GB, formatujemy (aby mieć dostęp do tych poleceń formatowania kliknąć ”Drive options”).  
Następnie przechodzimy do dalszej części instalacji przyciskiem *NEXT*.

Po zakończeniu instalacji (około 1,5h) konieczne jest ponownie wejście do BIOS i ustawienie nowej kolejności bootowania systemu:  
>BOOT / Legacy / Boot Type Order:  
- Hard Disk Drive  
- USB  
- Others  
- CD/DVD ROM Drive  
- Floppy Drive  

Instalacja zostanie dokończona już z karty Compact Flash.


### Instalacja dodatkowego oprogramowania
W celu zakończenia programowania Stacji Transmisji Danych należy wgrać dodatkowe oprogramowanie odpowiedzialne za zbieranie, magazynowanie oraz przesyłanie danych.  

Instrukcja wgrywania oraz programy instalacyjne znajdują się na dysku S:  
*S:\TR\Zimoch Szymon\ Wizualizacja*    
Osoba prowadząca: Szymon Zimoch.  


### Obsługa programów ochrony danych EWF oraz HORM
Enhanced Write Filter służy do ochrony danych na dysku twardym. 
Po załączeniu EWF wszystkie operacje wykonywane przez użytkownika na dysku (np. instalacja nowych programów) są przechwytywane i zapisywane w pamięci RAM.   
Po zrestartowaniu systemu nie zostanie więc żaden ślad po czynnościach wykonywanych w poprzedniej sesji systemu.

Dzięki HORM (Hibernate Once Resume Many) można zapisać zawartość pamięci do pliku hiberfile.sys. 
Daje to możliwość uruchamiania systemu za każdym razem z tego samego pliku przez co zawsze mamy do dyspozycji pulpit w stanie sprzed pierwszego zahibernowania.

Zarządzaniem funkcjami EWF oraz HORM zajmuje się aplikacja „Write File Manager”.  
WFM jest uruchamiany przy każdym starcie systemu w trayu (prawy dolny róg pulpitu) - ikonka czerwonej lub zielonej tarczy.  

W celu załączenia HORM należy wykonać kolejno następujące czynności:  
- *Commands / Disable* – w przypadku gdy EWF jest aktywny należy go wyłączyć,  
- *HORM / Activate HORM*,  
- *Commands / Enable* – włączenie EWF  
- *Reboot* – restart systemu  
- *HORM / Execute HORM* – zrzucenie pamięci do pliku. (UWAGA: Po kliknięciu tego rozkazu obraz się robi czarny – nie wyłączać zasilania jeszcze przez najmniej 20 sekund gdyż pamięć jest cały czas zrzucana do pliku!).  

Wyłączenie HORM nastąpi po kliknięciu Commands / Commit and Disable Live a następnie HORM / Deactivate HORM.  
Restart systemu nie jest konieczny. Operacje na plikach są ponownie zapisywane na dysku.


### Dodatkowe uwagi
Jeśli monitor jest programowany poza STD (np. na biurku w MEA) bez podłączonej docelowej klawiatury oraz myszki z STD należy pamiętać o zdjęciu EWF podczas montażu docelowego w stacji.

Przy pierwszym załączeniu zasilania stacji system Windows będzie instalował sterowniki PnP klawiatury i w przypadku włączonego EWF zapisze je tylko w pamięci RAM a nie na dysku.  
Spowoduje to, że po każdym ponownym restarcie zasilania system Windows będzie instalował te sterowniki od nowa!  
Także pierwsze uruchomienie należy wykonać ze zdjętym EWF oraz HORM.

