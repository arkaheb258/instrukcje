# Wstęp  
W tym miejscu są umieszczone wszystkie instrukcje do użytku wewnętrznego w zakładzie KM Zabrze.  


# Uwagi  
W przypadku nie działających linków w spisie treści (szczególnie na serwerze bonobo 192.168.3.30) należy nawigować bezpośrednio po katalogach.  

# Spis treści  
### GUŁ  
- [Instalacja systemu operacyjnego na monitorze WK5](gul/instalacjaTinyCoreNaWK5.md)  

### KTW  
- [Instalacja systemu operacyjnego na monitorze głównym - DC1](ktw/instalacjaTinyCoreNaDC1.md)  
- [Instalacja systemu operacyjnego na monitorze pomocniczym - Olimex](ktw/instalacjaLinuxNaOlimex.md)  

### STD  
- [Instalacja systemu operacyjnego Windows Embedded Standard 7](std/instalacjaWES7.md)  

### WOW  
- [Instalacja systemu operacyjnego Windows Embedded Standard 7](wow/instalacjaSystemuNaOlimex.md)  


### Wspólne  
- [Aktualizacja programu wizualizacji ](wspolne/aktualizacjaWizualizacji.md)  
- [Obsługa plików w formacie *.md ](wspolne/obslugaPlikowMD.md)  

    


